import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class UiUtilsProvider {

  constructor(private toastCtrl: ToastController) {
    
  }

  public showSuccessToast() {
    const toast = this.toastCtrl.create({
      message: 'Operação realizada com sucesso!',
      duration: 3000
    });
    toast.present();
  }

  public showErrorToast() {
    const toast = this.toastCtrl.create({
      message: 'Ocorreu um erro ao processar sua solicitação, tente novamente!',
      duration: 3000
    });
    toast.present();
  }

  public showSignupValidationToast() {
    const toast = this.toastCtrl.create({
      message: 'Verifique os campos antes de salvar sua conta!',
      duration: 3000
    });
    toast.present();
  }

  public showNotFoundUserToast() {
    const toast = this.toastCtrl.create({
      message: 'Usuário não localizado na nossa base. Experimente criar uma conta :)',
      duration: 3000
    });
    toast.present();
  }

  

}
