import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { GlobalProvider } from '../global/global';
import 'rxjs/add/operator/map';

@Injectable()
export class ContactsProvider {

  url: string;
  sufix: string;
  headers: Headers;

  constructor(public http: Http, private global: GlobalProvider) {
    this.url = global.BASE_URL;
    this.sufix = 'contacts';

    const stringUser = this.global.getLocalUser();
    if (stringUser) {
      const user = JSON.parse(stringUser);
      this.headers = new Headers();
      this.headers.set('Authorization', user.token);
    }

  }

  list() {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}/${this.sufix}`, {headers: this.headers})
        .map(res => res.json())
        .subscribe(data => {
          if (data && data.success === false) {
            reject(data.message);
          } else {
            resolve(data);
          }
        }, err => {
          reject(err);
        });
    });
  }

  create(contact) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.url}/${this.sufix}`, contact, {headers: this.headers})
        .map(res => res.json())
        .subscribe(data => {
          if (data && !data.errors) {
            resolve(data);
          } else {
            reject(data.errors);
          }
        }, err => {
          reject(err);
        });
    });
  }

  find(id) {
    return new Promise((resolve, reject) => {
      this.http.get(`${this.url}/${this.sufix}/${id}`, {headers: this.headers})
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  remove(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(`${this.url}/${this.sufix}/${id}`, {headers: this.headers})
        .subscribe(data => {
          resolve(data);
        }, err => {
          reject(err);
        });
    });
  }

  update(contact) {
    return new Promise((resolve, reject) => {
      this.http.put(`${this.url}/${this.sufix}/${contact._id}`, contact, {headers: this.headers})
        .map(res => res.json())
        .subscribe(data => {
          if (data && !data.errors) {
            resolve(data);
          }
        }, err => {
          reject(err);
        });
    });
  }

}
