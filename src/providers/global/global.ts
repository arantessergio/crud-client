import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GlobalProvider {

  public BASE_URL: string = 'http://node-crud-com.umbler.net';
  // public BASE_URL: string = 'http://localhost:3000';

  constructor(private http: Http) {

  }

  getLocalUser() {
    return window.localStorage.getItem('crud-user');
  }

  createAccount(user) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.BASE_URL}/users`, user)
        .map(res => res.json())
        .subscribe(data => {
          if (data && data._id) {



            resolve(data);
          } else {
            reject(data);
          }
        }, err => {
          reject(err);
        });
    });
  }

  login(user) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.BASE_URL}/users/authenticate`, user)
        .map(res => res.json())
        .subscribe(data => {
          if (data && data.token) {

            window.localStorage.setItem('crud-user', JSON.stringify(data));

            resolve(data);
          } else {
            reject(data);
          }
        }, err => {
          reject(err);
        });
    });
  }

}
