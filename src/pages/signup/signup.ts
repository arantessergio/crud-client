import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams } from 'ionic-angular';
import { GlobalProvider } from '../../providers/global/global';
import { UiUtilsProvider } from '../../providers/ui-utils/ui-utils';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  user: any;
  loading: boolean = false;

  constructor(public nav: Nav, public navParams: NavParams,
    private global: GlobalProvider, private utils: UiUtilsProvider) {
      this.user = {
        email: null,
        password: null,
        retypePassword: null
      };
  }

  save() {
    if (this.canSignup()) {
      this.loading = true;
      this.global.createAccount(this.user).then(response => {
        this.global.login(response).then(auth => {
          this.nav.setRoot(HomePage);
          this.loading = false;
        });
      }).catch(err => {
        this.utils.showErrorToast();
        this.loading = false;
      });
    } else {
      this.utils.showSignupValidationToast();
    }
  }

  canSignup() {
    return this.user && this.user.email && 
      this.user.email !== '' && this.user.password &&
      this.user.password !== '' && this.user.retypePassword &&
      this.user.retypePassword !== '' && (this.user.password === this.user.retypePassword);
  }

}
