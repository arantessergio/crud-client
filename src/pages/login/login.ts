import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { GlobalProvider } from '../../providers/global/global';
import { UiUtilsProvider } from '../../providers/ui-utils/ui-utils';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user: any;

  constructor(public nav: Nav, public navParams: NavParams, 
    private global: GlobalProvider, private utils: UiUtilsProvider) {

      this.user = {
        email: null,
        password: null
      }
  }

  createAccount() {
    this.nav.push(SignupPage);
  }

  login() {
    if (this.canLogin()) {
      this.global.login(this.user).then(response => {
        this.nav.push(HomePage);
      }).catch(err => {
        if (err && err.status === 404) {
          this.utils.showNotFoundUserToast();
        } else {
          this.utils.showErrorToast();
        }
      });
    } else {
      this.utils.showSignupValidationToast();
    }
  }

  canLogin() {
    return this.user && this.user.email && this.user.email !== '' && this.user.password && this.user.password !== '';
  }

}
