import { Component } from '@angular/core';
import { Nav, Events, ActionSheetController } from 'ionic-angular';
import { NewContactPage } from '../new-contact/new-contact';
import { ContactsProvider } from '../../providers/contacts/contacts';
import { UiUtilsProvider } from '../../providers/ui-utils/ui-utils';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loading: boolean = true;
  contacts: any;

  constructor(public nav: Nav, private contactsProvider: ContactsProvider,
    private events: Events, private actionSheetCtrl: ActionSheetController,
    private utils: UiUtilsProvider) {
      this.events.subscribe('update-contact-list', () => {
        this.loadContacts();
      });
      this.contacts = [];
      this.loadContacts();
  }

  loadContacts() {
    this.loading = true;
    this.contactsProvider.list().then(list => {
      this.contacts = list;
      this.loading = false;
    }).catch(err => {
      this.utils.showErrorToast();
      this.loading = false;
    });
  }

  newContact() {
    this.nav.push(NewContactPage);
  }

  showRemoveDialog(contact) {
    const actionSheet = this.actionSheetCtrl.create({
      title: `Deseja remover o contato ${contact.name}?`,
      buttons: [{
        text: 'Sim',
        role: 'destructive',
        handler: () => {
          this.remove(contact._id)
        }
      }, {
        text: 'Não',
        role: 'cancel'
      }]
    });

    actionSheet.present();
  }

  remove(id) {
    this.contactsProvider.remove(id).then(response => {
      this.utils.showSuccessToast();
      this.events.publish('update-contact-list');
    }).catch(err => {
      this.utils.showErrorToast();
    });
  }

  edit(id) {
    this.contactsProvider.find(id).then(response => {
      this.nav.push(NewContactPage, {
        from: 'editing',
        data: response
      });
    }).catch(err => {
      this.utils.showErrorToast();
    });
  }

}
