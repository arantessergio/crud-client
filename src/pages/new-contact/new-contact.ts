import { Component } from '@angular/core';
import { IonicPage, Nav, NavParams, Events } from 'ionic-angular';
import { ContactsProvider } from '../../providers/contacts/contacts';
import { UiUtilsProvider } from '../../providers/ui-utils/ui-utils';


@IonicPage()
@Component({
  selector: 'page-new-contact',
  templateUrl: 'new-contact.html',
})
export class NewContactPage {

  contact: any;
  from: any;

  constructor(public nav: Nav, public navParams: NavParams, 
      private contactsProvider: ContactsProvider,
      private events: Events,
      private utils: UiUtilsProvider) {

    this.from = navParams.get('from');
    this.contact = navParams.get('data');

    if (!this.contact) {
      this.contact = {
        name: null,
        email: null,
        birth: null,
        phoneNumber: null
      };
    }
  }

  save() {
    if (this.from && this.from === 'editing') {
      this.contactsProvider.update(this.contact).then(response => {
        this.utils.showSuccessToast();
        this.events.publish('update-contact-list');
      }).catch(err => {
        this.utils.showSuccessToast();
      });
    } else {
      this.contactsProvider.create(this.contact).then(response => {
        this.events.publish('update-contact-list');
        this.utils.showSuccessToast();
      }).catch(err => {
        this.utils.showErrorToast();
      });
    }
  }

}
