import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePageModule } from '../pages/home/home.module';
import { NewContactPageModule } from '../pages/new-contact/new-contact.module';
import { LoginPageModule } from '../pages/login/login.module';
import { SignupPageModule } from '../pages/signup/signup.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactsProvider } from '../providers/contacts/contacts';
import { UiUtilsProvider } from '../providers/ui-utils/ui-utils';
import { GlobalProvider } from '../providers/global/global';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    HomePageModule,
    NewContactPageModule,
    LoginPageModule,
    SignupPageModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactsProvider,
    UiUtilsProvider,
    GlobalProvider
  ]
})
export class AppModule {}
